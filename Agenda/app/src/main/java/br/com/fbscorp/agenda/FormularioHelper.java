package br.com.fbscorp.agenda;

import android.widget.EditText;
import android.widget.RatingBar;

import br.com.fbscorp.agenda.modelo.Contato;

/**
 * Created by felipe on 29/10/17.
 */

public class FormularioHelper {

    private final RatingBar nota;
    private final EditText nome;
    private final EditText endereco;
    private final EditText telefone;

    public FormularioHelper(FormularioActivity activity){
        nome = (EditText) activity.findViewById(R.id.formulario_nome);
        endereco = (EditText) activity.findViewById(R.id.formulario_endereco);
        telefone = (EditText) activity.findViewById(R.id.formulario_telefone);
        nota = (RatingBar) activity.findViewById(R.id.formulario_nota);
    }

    public Contato pegaContato() {
        Contato contato = new Contato();
        contato.setNome(nome.getText().toString());
        contato.setEndereco(endereco.getText().toString());
        contato.setTelefone(telefone.getText().toString());
        contato.setNota(Double.valueOf(nota.getProgress()));
        return contato;
    }
}
